#!/bin/bash
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install -y lxc git ansible lxd tmux wget jq
sudo mkdir /images
sudo chmod 777 /images
lxd init --auto

echo
echo "Would you like to download the base-1804-nocuda image? "
read -p "y or n: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    wget https://storage.googleapis.com/pawr-cm/base-1804-nocuda.tar.gz
    lxc image import base-1804-nocuda.tar.gz  --alias base-1804-nocuda
    rm base-1804-nocuda.tar.gz
fi


echo
echo "Would you like to download the col-baseline image? "
read -p "y or n: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    wget https://storage.googleapis.com/pawr-cm/images/col-baseline.tar.gz
    lxc image import col-baseline.tar.gz  --alias col-baseline
    rm col-baseline.tar.gz
fi

echo
echo "Would you like to download the col-baseline-oai image? "
read -p "y or n: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    wget https://storage.googleapis.com/pawr-cm/images/col-baseline-oai.tar.gz
    lxc image import col-baseline-oai.tar.gz  --alias col-baseline-oai
    rm col-baseline-oai.tar.gz
fi

FILE=$HOME/.ssh/colosseum-build
if [ ! -f "$FILE" ]; then
    echo
    echo "Would you like to create colosseum-build key in ~/.ssh?"
    echo "This key will be used to access your radio container; the build scripts will not work without it."
    read -p "y or n: " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        ssh-keygen -f $FILE -N ''
    fi
fi

FILE=$HOME/.ssh/colosseum
if [ ! -f "$FILE" ]; then
    echo
    echo "Would you like to create colosseum key in ~/.ssh?"
    echo "This key will be used to access Colosseum; the build scripts will not work without it."
    read -p "y or n: " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        ssh-keygen -f $FILE -N ''
    fi
fi

echo
echo "Bootstraping scripts with your team credntials"
echo

./bootstrap.sh

echo
echo "Would you like to create ssh config entries in ~/.ssh/config? (required if entries do not already exist)"
read -p "y or n: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
   touch ~/.ssh/config
   cat ssh_config >> ~/.ssh/config
   sudo chmod 644 ~/.ssh/config
fi

echo
echo "Provisioning complete.  Next steps:"
echo "  1) After copying your keys to the Colosseum web interface, add your colosseum.pub to authorized_keys on sc2-lz, then run ssh sc2-lz to verify that you have password-free connectivity. See README.md for more details."
echo "  2) run ansible-playbook baseline-update-ssh.yml in the ansible directory to add your build key to the baseline image."
echo "  3) run ansible-playbook srs.yml in the ansible directory to build your radio image, push to Colossuem, and run a batch job."
echo
echo
