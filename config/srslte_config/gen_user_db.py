#!/usr/bin/env python3
import argparse

HEADER = "#\n" +\
         "# .csv to store UE's information in HSS\n" +\
         "# Kept in the following format: \"Name,Auth,IMSI,Key,OP_Type,OP,AMF,SQN,QCI,IP_alloc\"\n" +\
         "#\n" +\
         "# Name:     Human readable name to help distinguish UE's. Ignored by the HSS\n" +\
         "# IMSI:     UE's IMSI value\n" +\
         "# Auth:     Authentication algorithm used by the UE. Valid algorithms are XOR\n" +\
         "#           (xor) and MILENAGE (mil)\n" +\
         "# Key:      UE's key, where other keys are derived from. Stored in hexadecimal\n" +\
         "# OP_Type:  Operator's code type, either OP or OPc\n" +\
         "# OP/OPc:   Operator Code/Cyphered Operator Code, stored in hexadecimal\n" +\
         "# AMF:      Authentication management field, stored in hexadecimal\n" +\
         "# SQN:      UE's Sequence number for freshness of the authentication\n" +\
         "# QCI:      QoS Class Identifier for the UE's default bearer.\n" +\
         "# IP_alloc: IP allocation stratagy for the SPGW.\n" +\
         "#           With 'dynamic' the SPGW will automatically allocate IPs\n" +\
         "#           With a valid IPv4 (e.g. '172.16.0.2') the UE will have a statically assigned IP.\n" +\
         "#\n" +\
         "# Note: Lines starting by '#' are ignored and will be overwritten\n"

def gen_user_db(filename="user_db.csv", num_ues=228, append=False, amf_start=0x9000, sequence_start=0x12b7, ip_offset=0, start_ue=2):
    dbfile = open(filename, "w+" if append else "w")
    if not append:
        dbfile.write(HEADER)
    for ue in range(start_ue, num_ues+start_ue):
        dbfile.write("ue{},xor,{:03d}010123456789,{:03d}12233445566778899aabbccddeeff,opc,63bfa50ee6523365ff14c1f45f88737d,{:04x},{:012x},7,172.16.0.{}\n".format(
            ue, ue, ue, ue+amf_start, ue+sequence_start, ue+ip_offset))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", default="user_db.csv",
                        help="Filename to write UE database to")
    parser.add_argument("-a", "--append", action="store_true",
                        help="Append current file, if not set file will be overwritten")
    parser.add_argument("-n", "--num-ues", type=int, default=228,
                        help="Number of UEs")
    parser.add_argument("-o", "--ip-offset", type=int, default=0,
                        help="IP offset for last octet of first UE")
    parser.add_argument("-s", "--start-ue", type=int, default=2,
                        help="First UE to put in list")
    args = parser.parse_args()
    gen_user_db(args.filename, args.num_ues, args.append, ip_offset=args.ip_offset, start_ue=args.start_ue)
