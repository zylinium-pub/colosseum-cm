#!/bin/bash
read -p 'Colosseum team name: ' team_name
read -p 'Colosseum user name: ' user_name
read -p 'Colosseum user password: ' user_password
read -p 'Container SSH key: ' -i colosseum-build -e col_ssh_key
sed "s/<< team_name >>/${team_name}/g" ansible/srs.yml.in > ansible/srs.yml
sed -i "s/<< ssh_key >>/${col_ssh_key}/g" ansible/srs.yml
sed "s/<< team_name >>/${team_name}/g" ansible/oai.yml.in > ansible/oai.yml
sed -i "s/<< ssh_key >>/${col_ssh_key}/g" ansible/oai.yml
sed "s/<< user_name >>/${user_name}/g" ansible/curl.py.in > ansible/curl.py
sed "s/<< team_name >>/${team_name}/g" ssh_config.in > ssh_config
sed -i "s/<< user_name >>/${user_name}/g" ssh_config
sed -i "s/<< user_password >>/${user_password}/g" ansible/curl.py
