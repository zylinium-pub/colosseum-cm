# Ansible playbook for generating a Colosseum baseline image with standard packages installed
- hosts: localhost
  vars:
    container_name: col-baseline
    base_image: base-1804-nocuda
    images_dir: /images
    sha_file: /root/gitsha
    uhd_version: release_003_009_007
    gnuradio_version: 3.8.1.0
    colosseumcli_version: 18.05.0
    protobuf_version: 3.7.1
    root_password: ChangeMe
  tasks:
  - name: Delete the container if it exists
    lxd_container:
      name: "{{ container_name }}"
      state: absent

  - name: Delete image if it exists
    command: lxc image delete {{ container_name }}
    ignore_errors: yes

  - name: Create container
    lxd_container:
      name: "{{ container_name }}"
      state: started
      source:
        type: image
        alias: "{{ base_image }}"
      profiles: ["default"]
      wait_for_ipv4_addresses: true
      timeout: 600

  - name: Add Host
    add_host:
      name: "{{ container_name }}"
      ansible_connection: lxd

  - name: set root password
    delegate_to: "{{ container_name }}"
    user:
      name: root
      password: "{{ root_password | password_hash('sha512') }}"

  - name: Disable unnecessary systemd services and timers
    delegate_to: "{{ container_name }}"
    systemd:
      name: "{{ item }}"
      enabled: no
      masked: yes
      state: stopped
    with_items:
    - apt-daily-upgrade.timer
    - apt-daily.timer
    - systemd-tmpfiles-clean.timer
    - unattended-upgrades
    - cron
    - apt-daily.service
    - apt-daily-upgrade.service

  - name: apt upgrade
    delegate_to: "{{ container_name }}"
    apt: update_cache=yes upgrade=yes autoremove=yes

  - name: Install packages
    delegate_to: "{{ container_name }}"
    apt:
      pkg: ['git', 'python3-mako', 'python3-pip', 'python-lxml', 'python-pip', 'python-gtk2', 'libfreetype6-dev', 'ipython3', 'liblapack-dev', 'fortran-compiler', 'emacs', 'htop', 'unzip', 'autoconf', 'automake', 'libtool', 'curl', 'libqt4-dev', 'libgmp-dev', 'pkg-config', 'autoconf', 'libtool', 'zlib1g-dev', 'lsb-release', 'iperf', 'liblog4cpp5-dev', 'libfftw3-dev', 'libmbedtls-dev', 'libboost-program-options-dev', 'libconfig++-dev', 'libsctp-dev', 'libczmq-dev', 'libgtk-3-dev', 'cmake', 'python3-zmq', 'python3-scipy', 'python3-gi', 'python3-gi-cairo']
      state: present

  - name: pip3 install
    delegate_to: "{{ container_name }}"
    command: pip3 install {{ item }}
    with_items:
    - pandas
    - pytest

  - name: Clone gpsd
    delegate_to: "{{ container_name }}"
    git:
      repo: https://git.savannah.nongnu.org/git/gpsd.git
      dest: gpsd
    register: gitresult

  - name: Save gpsd git sha to file
    delegate_to: "{{ container_name }}"
    lineinfile:
      path: "{{ sha_file }}"
      state: present
      create: yes
      line: "gpsd: {{ gitresult.after }}"

  - name: Build gpsd
    delegate_to: "{{ container_name }}"
    command: chdir=gpsd {{ item }}
    with_items:
    - scons
    - scons check
    - scons udev-install

  - name: Clone Google protobuf {{ protobuf_version }} from github
    delegate_to: "{{ container_name }}"
    git:
      repo: https://github.com/google/protobuf
      dest: protobuf
      version: v{{ protobuf_version }}
    register: gitresult

  - name: Save protobuf git sha to file
    delegate_to: "{{ container_name }}"
    lineinfile:
      path: "{{ sha_file }}"
      state: present
      create: yes
      line: "protobuf: {{ gitresult.after }}"

  - name:  Build protobuf
    delegate_to: "{{ container_name }}"
    command: chdir=protobuf {{ item }}
    with_items:
    - ./autogen.sh
    - ./configure
    - make -j{{ ansible_processor_vcpus }}
    - make -j{{ ansible_processor_vcpus }} check
    - make install
    - ldconfig

  - name: Remove protobuf installer
    delegate_to: "{{ container_name }}"
    file:
      state: absent
      path: /root/protobuf

  - name: Remove old uhd include/lib
    delegate_to: "{{ container_name }}"
    file:
      state: absent
      path: "{{ item }}"
    with_items:
    - /usr/local/include/uhd
    - /usr/local/lib/uhd
    - /root/uhd

  - name: Remove old uhd .so
    delegate_to: "{{ container_name }}"
    file:
      state: absent
      path: "{{ item }}"
    with_fileglob:
    - "/usr/local/lib/libuhd.so*"

  - name: Clone uhd {{ uhd_version }} from github
    delegate_to: "{{ container_name }}"
    git:
      repo: https://github.com/EttusResearch/uhd.git
      dest: uhd
      recursive: yes
      accept_hostkey: yes
      version: "{{ uhd_version }}"
    register: gitresult

  - name: Save uhd git sha to file
    delegate_to: "{{ container_name }}"
    lineinfile:
      path: "{{ sha_file }}"
      state: present
      create: yes
      line: "uhd: {{ gitresult.after }}"

  - name: create uhd/host/build
    delegate_to: "{{ container_name }}"
    file:
      path: uhd/host/build
      state: directory

  - name: Build uhd
    delegate_to: "{{ container_name }}"
    command: chdir=uhd/host/build {{ item }}
    with_items:
    - cmake ../
    - make -j{{ ansible_processor_vcpus }} install
    - ldconfig

  - name: Download FPGA image
    delegate_to: "{{ container_name }}"
    command: uhd_images_downloader

  - name: Clone gnuradio {{ gnuradio_version }} from github
    delegate_to: "{{ container_name }}"
    git:
      repo: https://github.com/gnuradio/gnuradio.git
      dest: gnuradio
      version: v{{ gnuradio_version }}
    register: gitresult

  - name: Save gnuradio git sha to file
    delegate_to: "{{ container_name }}"
    lineinfile:
      path: "{{ sha_file }}"
      state: present
      create: yes
      line: "gnuradio: {{ gitresult.after }}"

  - name: Create gnuradio/build
    delegate_to: "{{ container_name }}"
    file:
      path: gnuradio/build
      state: directory

  - name: Build gnuradio
    delegate_to: "{{ container_name }}"
    command: chdir=gnuradio/build {{ item }}
    with_items:
    - cmake ../
    - make -j{{ ansible_processor_vcpus }} install
    - ldconfig

  - name: Add PYTHONPATH path to .bashrc
    delegate_to: "{{ container_name }}"
    lineinfile:
      path: /root/.bashrc
      line: export PYTHONPATH=/usr/local/lib/python3/dist-packages:/usr/local/lib/python3/site-packages

  - name: Load colosseum-cli
    delegate_to: "{{ container_name }}"
    unarchive:
      src: "{{ images_dir }}/colosseumcli-{{ colosseumcli_version }}-2.tar.gz"
      dest: /root

  - name: Build colosseum-cli
    delegate_to: "{{ container_name }}"
    command: chdir=colosseumcli-{{ colosseumcli_version }} python3 setup.py install

  - name: Delete large directories
    delegate_to: "{{ container_name }}"
    file:
      path: "{{ item }}"
      state: absent
    with_items:
    - /root/gnuradio
    - /root/protobuf
    - /root/gpsd
    - /root/uhd
    - /usr/local/share/doc

  - name: Clean apt cache
    delegate_to: "{{ container_name }}"
    command: apt-get clean

  - name: Stop container
    lxd_container:
      name: "{{ container_name }}"
      state: stopped

  - name: Publish container
    command: lxc publish {{ container_name }} --alias {{ container_name }}

  - name: Export image
    command: lxc image export {{ container_name }} {{ images_dir }}/{{ container_name }}

  - name: Delete the container
    lxd_container:
      name: "{{ container_name }}"
      state: absent
