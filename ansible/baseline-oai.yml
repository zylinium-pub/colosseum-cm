# Ansible playbook for generating a Colosseum baseline image with standard packages installed
- hosts: localhost
  vars:
    container_name: col-baseline-oai
    base_image: col-baseline
    images_dir: /images
    sha_file: /root/gitsha
    oai_version: develop
    uname_template: uname.j2
    kernel_version: 4.4.0-109-generic
  tasks:
  - name: Delete the container if it exists
    lxd_container:
      name: "{{ container_name }}"
      state: absent

  - name: Delete image if it exists
    command: lxc image delete {{ container_name }}
    ignore_errors: yes

  - name: Create container
    lxd_container:
      name: "{{ container_name }}"
      state: started
      source:
        type: image
        alias: "{{ base_image }}"
      profiles: ["default"]
      wait_for_ipv4_addresses: true
      timeout: 600

  - name: Add Host
    add_host:
      name: "{{ container_name }}"
      ansible_connection: lxd

  - name: Copy deb files for linux headers
    delegate_to: "{{ container_name }}"
    copy:
      src: "{{ item }}"
      dest: /root
    with_items:
    - ../deb/linux-headers-4.4.0-109_4.4.0-109.132_all.deb
    - ../deb/linux-headers-4.4.0-109-generic_4.4.0-109.132_amd64.deb

  - name: Install linux headers for {{ kernel_version }}
    delegate_to: "{{ container_name }}"
    apt:
      deb: "{{ item }}"
    with_items:
    - /root/linux-headers-4.4.0-109_4.4.0-109.132_all.deb
    - /root/linux-headers-4.4.0-109-generic_4.4.0-109.132_amd64.deb

  - name: Move uname
    delegate_to: "{{ container_name }}"
    command: mv /bin/uname /bin/uname.orig

  - name: Create new uname for {{ kernel_version }}
    delegate_to: "{{ container_name }}"
    template:
      src: "{{ uname_template }}"
      dest: /bin/uname
      mode: 0755

  - name: Clone OAI from gitlab
    delegate_to: "{{ container_name }}"
    git:
      repo: https://gitlab.eurecom.fr/oai/openairinterface5g.git
      dest: openairinterface5g
      accept_hostkey: yes
      version: "{{ oai_version }}"
    register: gitresult

  - name: Save OAI git sha to file
    delegate_to: "{{ container_name }}"
    lineinfile:
      path: "{{ sha_file }}"
      state: present
      create: yes
      line: "OAI: {{ gitresult.after }}"

  - name: Build OAI dependencies
    delegate_to: "{{ container_name }}"
    shell: ". openairinterface5g/oaienv && openairinterface5g/cmake_targets/build_oai -I"

  - name: Remove OAI directory
    delegate_to: "{{ container_name }}"
    file:
      path: /root/openairinterface5g
      state: absent

  - name: Stop container
    lxd_container:
      name: "{{ container_name }}"
      state: stopped

  - name: Publish container
    command: lxc publish {{ container_name }} --alias {{ container_name }}

  - name: Export image
    command: lxc image export {{ container_name }} {{ images_dir }}/{{ container_name }}

  - name: Delete the container
    lxd_container:
      name: "{{ container_name }}"
      state: absent
