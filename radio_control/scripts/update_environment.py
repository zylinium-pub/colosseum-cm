#!/usr/bin/env python3
import sys
from radio_control.client import update_environment

if len(sys.argv) > 1:
    env = sys.argv[1]
else:
    env = '/root/radio_api/environment.json'

update_environment(env)
