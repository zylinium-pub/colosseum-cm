#!/usr/bin/env python3
import daemon

from radio_control.server import run_server

with daemon.DaemonContext():
     run_server()
