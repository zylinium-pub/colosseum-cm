#!/usr/bin/env python3
import argparse
import time
import subprocess
import os
import netifaces
import json

import logging
logging.basicConfig(format='[%(asctime)s.%(msecs)03d %(levelname)s] %(message)s', datefmt='%Y-%m-%d,%H:%M:%S',
                    level=logging.INFO)

def get_arguments(cmd):
    args = []
    cmd_out = subprocess.check_output([cmd, "--help"])
    for argline in str(cmd_out).split('\\n'):
        argwords = argline.split()
        if len(argwords) > 0 and argwords[0].startswith('--'):
            args.append(argwords[0])
    return args

def srs_start(cmd, config_file, extra_args=[], logfile=None):
    srs_out = None
    if logfile:
        try:
            srs_out = open(logfile, "w")
        except:
            logging.warning("Unable to open file for logging {}: {}".format(cmd, logfile))

    valid_args = get_arguments(cmd)
    srs_args = [cmd, config_file]
    argidx = 0
    while argidx < (len(extra_args) - 1):
        if extra_args[argidx] in valid_args:
            logging.info("Adding {}={} to {} arguments".format(extra_args[argidx], extra_args[argidx+1], cmd))
            srs_args.extend(extra_args[argidx:argidx+2])
        else:
            logging.info("Ignoring argument {} for {}".format(extra_args[argidx], cmd))
        argidx += 2

    proc = subprocess.Popen(srs_args, stdout=srs_out, stderr=srs_out,
                            cwd=os.path.dirname(config_file))
    logging.info("{} started, config file: {}, pid: {}".format(cmd, config_file, proc.pid))

    return proc

def run_srs(args, extra_args=[]):
    logging.info("Starting SRS, args: {}, extra: {}".format(args, extra_args))

    if args.startfile:
        f = open(args.startfile, 'w')
        f.write('STARTED')
        f.close()
        logging.info('Wrote STARTED to: {}'.format(args.startfile))

        logging.info("Waiting for radio to start, monitoring: {}".format(args.startfile))
        while True:
            try:
                f = open(args.startfile, "r")
                if f.read() == "START":
                    break
            except:
                pass
            time.sleep(0.1)

    logging.info("Starting radio")

    procs = []
    completed_procs = []

    # Run EPC and eNodeB if master
    if args.master:
        procs.append(srs_start("srsepc", args.epc_config, extra_args, args.epc_log))
        time.sleep(5)
        procs.append(srs_start("srsenb", args.enb_config, extra_args, args.enb_log))

        # Wait for network interface to start
        while "srs_spgw_sgi" not in netifaces.interfaces():
            time.sleep(0.5)

        logging.info("srs_spgw_sgi interface created: {}".format(netifaces.ifaddresses('srs_spgw_sgi')))

        if args.tcpdump:
            try:
                tcpdump_out = open("/logs/tcpdump.log", "w")
            except:
                tcpdump_out = None
                logging.warning("Unable to open file for logging tcpdump: /logs/tcpdump.log")
            procs.append(subprocess.Popen(["tcpdump", "-i", "srs_spgw_sgi", "-w", args.tcpdump],
                                          stdout=tcpdump_out, stderr=tcpdump_out))
            logging.info("tcpdump started, logging to: {}, pid: {}".format(args.tcpdump, procs[-1].pid))

        if args.iperf:
            time.sleep(3)
            try:
                iperf_out = open("/logs/iperf.log", "w")
            except:
                iperf_out = None
                logging.warning("Unable to open file for logging iperf: /logs/iperf.log")
            procs.append(subprocess.Popen(["iperf", "-s"], stdout=iperf_out, stderr=iperf_out))
            logging.info("iperf server started, pid: {}".format(procs[-1].pid))

    else:
        # Give some time for enb to start
        time.sleep(10)

        # Set k/imsi/imei based on the node number
        extra_args.extend(["--usim.k", "{:03d}12233445566778899aabbccddeeff".format(args.node),
                           "--usim.imsi", "{:03d}010123456789".format(args.node),
                           "--usim.imei", "353490069873{:03d}".format(args.node)])
        procs.append(srs_start("srsue", args.ue_config, extra_args, args.ue_log))

        # Wait for network interface to start
        while "tun_srsue" not in netifaces.interfaces():
            time.sleep(0.5)

        logging.info("tun_srsue interface created: {}".format(netifaces.ifaddresses('tun_srsue')))

        if args.tcpdump:
            try:
                tcpdump_out = open("/logs/tcpdump.log", "w")
            except:
                tcpdump_out = None
                logging.warning("Unable to open file for logging tcpdump: /logs/tcpdump.log")
            procs.append(subprocess.Popen(["tcpdump", "-i", "tun_srsue", "-w", args.tcpdump],
                                          stdout=tcpdump_out, stderr=tcpdump_out))
            logging.info("tcpdump started, logging to: {}, pid: {}".format(args.tcpdump, procs[-1].pid))
            time.sleep(1)

        subprocess.call(["ping", "172.16.0.1", "-c", "10"])

        if args.iperf:
            try:
                iperf_out = open("/logs/iperf.log", "w")
            except:
                iperf_out = None
                logging.warning("Unable to open file for logging iperf: /logs/iperf.log")
            procs.append(subprocess.Popen(["iperf", "-c", "172.16.0.1", "-t", str(args.iperf)],
                                          stdout=iperf_out, stderr=iperf_out))
            logging.info("iperf client started, running for {} seconds, pid: {}".format(args.iperf, procs[-1].pid))

    if args.traffic:
        logging.info("Traffic configuration: {}".format(args.traffic))
        conf_file = open("/logs/traffic.conf", "w")
        for tc in eval(args.traffic):
            conf_file.write("{}\n".format(json.dumps(tc)))
        conf_file.close()

        try:
            traf_out = open("/logs/traffic_gen.log", "w")
        except:
            traf_out = None
            logging.warning("Unable to open file for logging traffic: /logs/traffic_gen.log")
        procs.append(subprocess.Popen(["traffic_gen.py", "-i", "/logs/traffic.conf",
                                       "-o", "/logs/traffic.log", "-n", str(args.node), "-t", str(args.team)],
                                      stdout=traf_out, stderr=traf_out))
        logging.info("Traffic generator started, pid: {}".format(procs[-1].pid))

    logging.info("Radio started")

    if args.startfile:
        f = open(args.startfile, 'w')
        f.write('STARTED')
        f.close()
        logging.info('Wrote STARTED to: {}'.format(args.startfile))

    # Wait for stop
    while True:
        try:
            f = open(args.startfile, "r")
            if f.read() == "STOP":
                break
        except:
            pass

        for proc in procs:
            if proc.pid not in completed_procs:
                retcode = proc.poll()
                if retcode is not None:
                    logging.info("Process with pid: {} has exited, retcode: {}".format(proc.pid, retcode))
                    completed_procs.append(proc.pid)
        time.sleep(0.1)

    logging.info("Stopping radio")
    for proc in procs:
        if proc.pid not in completed_procs:
            logging.info('Terminating process: {}'.format(proc.pid))
            try:
                proc.terminate()
            except:
                logging.warning('Caught exception trying to terminate process {}'.format(proc.pid))

    if args.startfile:
        f = open(args.startfile, 'w')
        f.write('STOPPED')
        f.close()

    logging.info("Radio stopped")

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--master", default=0, type=int,
                        help="Run this node as the master (eNB)")
    parser.add_argument("--iperf", default=0, type=int,
                        help="Run iperf for specified number of seconds")
    parser.add_argument("--traffic", default=None,
                        help="traffic generator configuration")
    parser.add_argument("--epc-config", default="/root/srslte_config/epc.conf",
                        help="srsepc configuration file")
    parser.add_argument("--epc-log", default="/logs/srsepc.log",
                        help="file to log srsepc stdout to")
    parser.add_argument("--enb-config", default="/root/srslte_config/enb.conf",
                        help="srsenb configuration file")
    parser.add_argument("--enb-log", default="/logs/srsenb.log",
                        help="file to log srsenb stdout to")
    parser.add_argument("--ue-config", default="/root/srslte_config/ue.conf",
                        help="srsue configuration file")
    parser.add_argument("--ue-log", default="/logs/srsue.log",
                        help="file to log srsue stdout to")
    parser.add_argument("--startfile", default=None,
                        help="file to monitor for radio start")
    parser.add_argument("--node", type=int, default=-1,
                        help="Node number for this node")
    parser.add_argument("--tcpdump", default=None,
                        help="File to dump tun device pcap to")
    parser.add_argument("--team", default=None,
                        help="team name")
    args, extra_args = parser.parse_known_args()
    run_srs(args, extra_args)

if __name__ == "__main__":
    main()
