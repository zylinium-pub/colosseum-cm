#!/usr/bin/env python3
import sys
from radio_control.client import boot

if len(sys.argv) > 1:
    conf = sys.argv[1]
else:
    conf = '/root/radio_api/radio.conf'

boot(conf)
