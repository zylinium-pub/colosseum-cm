"""Radio Control"""
from setuptools import setup

setup(
    name="radio_control",
    version="0.4.0",
    author="Roy Thompson",
    description="radio control library ",
    install_requires=['python-daemon==2.1.2',
                      'zmq',
                      'protobuf==3.7.1',
                      'netifaces'],
    scripts=['scripts/radiod.py',
             'scripts/radio_boot.py',
             'scripts/radio_start.py',
             'scripts/radio_status.py',
             'scripts/radio_stop.py',
             'scripts/radio_wait.py',
             'scripts/publish_mo.py',
             'scripts/update_environment.py',
             'scripts/run_srs.py',
             'scripts/run_oai.py',
             'scripts/traffic_gen.py'],
    packages=['radio_control'])
